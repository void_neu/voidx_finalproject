/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 package Business.Role;

/**
 *
 * @author Pankaj Gorav
 */
public abstract class Role {
    
    public enum RoleType{
    AdminRole("Admin"),    
    DistributorRole("DistributorRole"),
    QualityCheckRole("QualityCheckRole"),
    ShelterRole("ShelterRole"),
    SupplierRole("SupplierRole");
    
    
    
     private String value;
        private RoleType(String value){
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value;
        }
    
    }
    
    
    
}

