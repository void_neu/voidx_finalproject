/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import java.util.ArrayList;

/**
 *
 * @author Team Void
 */
public class OrganizationDirectory {
    
     private ArrayList<Organization> organizationList;

    public OrganizationDirectory() {
        organizationList = new ArrayList();
    }

    public ArrayList<Organization> getOrganizationList() {
        return organizationList;
    }
    
    public Organization createOrganization(Organization.Type type){
        Organization organization = null;
        if (type.getValue().equals(Organization.Type.Shelter.getValue())){
            organization = new ShelterOrganization();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Organization.Type.Distributor.getValue())){
            organization = new DistributorOrganization();
            organizationList.add(organization);
        }    
        else if (type.getValue().equals(Organization.Type.QualityCheck.getValue())){
            organization = new QualityOrganization();
            organizationList.add(organization);
        }
        
        else if (type.getValue().equals(Organization.Type.Supplier.getValue())){
            organization = new SupplierOrganization();
            organizationList.add(organization);
        }    
        
        return organization;
    }
    
}
